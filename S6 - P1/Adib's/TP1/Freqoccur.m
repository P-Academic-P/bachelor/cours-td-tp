%-- Extraire les différents symbols et leurs fréquences.
function [Symbols, Prob] = Freqoccur(Sequenc)

SortedSequenc = sort(Sequenc);
Out = RLEV3(SortedSequenc);
Symbols = Out.symbol;
Prob = Out.occur/length(Sequenc);
end