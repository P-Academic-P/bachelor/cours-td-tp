function Out = RLEdecodage(Inp)
Out = [];
Symbols = Inp.symbol;
Occur = Inp.occur;
for i = 1 : length(Symbols)
    for j = 1 : Occur(i)
        Out = [Out Symbols(i)];
    end
end