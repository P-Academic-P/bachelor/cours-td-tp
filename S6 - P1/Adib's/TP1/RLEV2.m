function Output = RLEV2(M)
Output = struct('symbol', {}, 'occur', {});
j = 0;
len = length(M);
i = 1;
while(i < len)
    count = 1;
    while(M(i) == M(i + 1) && i < len - 1)
        count = count + 1;
        i = i + 1;
    end
    j = j + 1;
    Output(j).occur = count;
    Output(j).symbol = M(i);
    i = i + 1;
end
end