                 %--|   Exercice 2   |---%
                 
Sequence = '12442131313';
% Extraire les symbols et leurs occurences.
[Symbols, Prob] = Freqoccur(Sequence)

% Cr�ation du dictionnaire de Huffman.
[Dictio, Avg] = huffmandict(Symbols, Prob)

% Taux de compression.
Occur = Prob * length(Sequence)
Codes = Dictio(:, 2)
TailleComp = 0;
for i = 1:length(Codes)
    TailleComp = TailleComp + length(Codes(i)) * Occur(i);
end
TauxDeCompression = 1 -  TailleComp / (length(Sequence) * 8)

% Longueur moyenne des bits pour l'ASCII.
LongMoyASCII = 8 * length(Sequence)

% Longueur moyenne des bits pour le codage de Huffman.
LongMoyHUFFM = sum(Occur .* Prob)/sum(Occur)

% Coder une suite.
Suite = '134441321344413213444132134441321344413213444132';
Out = Coder(Suite, Dictio)

