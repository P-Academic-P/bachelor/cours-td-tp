%-- Calculer le taux de compression.
function Taux = CalculTaux(Inp, Sequence)

Taux = 1 - length(Inp.occur) / length(Sequence);
end