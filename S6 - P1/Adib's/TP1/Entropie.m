%-- Calculer l'entropie.
function Ent = Entropie(Inp, Sequence)

Out = RLEV3(Sequence);
Len = length(Sequence);
P = Inp.occur/Len;
Ent = - P * log2(P).';
end