                 %--|   Exercice 1   |---%
Sequence = '12442131313';
% Retourner la suite des tuples.
Output = RLEV3(Sequence)

% Calculer l'entropie.
E = Entropie(Output, Sequence)

% Calculer le taux de compression.
T = CalculTaux(Output, Sequence)

% Decoder.
Out = RLEdecodage(Output)