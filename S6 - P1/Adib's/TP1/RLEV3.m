% -- Retourner et afficher les suites des tuples.
function Output = RLEV3(M)
i = 1;
j = 0;
count = 1;
while i < length(M)
    if M(i) == M(i + 1)
        count = count + 1;
    else
        j = j + 1;
        Occur(j) = count;
        Symbol(j) = M(i);
        disp(['( ' Symbol(j) ', ' num2str(Occur(j)) ' )']);
        count = 1;
    end
    i = i + 1;
end
    j = j + 1;
    Occur(j) = count;
    Symbol(j) = M(i);
    disp(['( ' Symbol(j) ', ' num2str(Occur(j)) ' )']);
    Symbol = Symbol - '0';
    Output = struct('symbol', Symbol, 'occur', Occur);
end
