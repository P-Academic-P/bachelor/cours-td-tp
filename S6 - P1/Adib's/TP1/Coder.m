function Out = Coder(Suite, Dictio)
Out = [];
for i = 1 : length(Suite)
    Out = [Out cell2mat(Dictio(str2num(Suite(i)), 2))];
end