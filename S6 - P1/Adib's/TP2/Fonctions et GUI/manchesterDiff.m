function y = manchesterDiff(sequence)
v = 5;
y = [];
if sequence(1) == '1'
    y = [y v -v];
else
    y = [y -v v];
end
for i = 2 : length(sequence)
    if sequence(i) == '1'
        y = [y -y(end - 1 : end)];
    else
        y = [y y(end - 1 : end)];
    end
end
if length(sequence) > 300
    max = 300;
else
    max = length(sequence);
end
stairs(0 : 0.5 : length(sequence), [y y(end)]);
axis([0 max + 1 -10 10])
end