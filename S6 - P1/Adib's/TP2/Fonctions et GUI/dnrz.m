function sequence = dnrz(y)

for i = 1 : length(y)
    if y(i) == 5
        sequence(i) = '1';
    else
        sequence(i) = '0';
    end
end
end