
                %------%
                % Main %
                %------%

% Choisir un fichier.                
[filename, pathname] = uigetfile('*', 'Choisir un fichier');
if  isequal(filename, 0)
    warndlg('vous n''avez rien choisi -_-');
end
type = 'image';

% Convertir le fichier.
data = convertir(filename, type);

% Recup�rer le fichier.
I = inverser(data, type);