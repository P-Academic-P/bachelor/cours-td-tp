function varargout = Figure(varargin)
% FIGURE MATLAB code for Figure.fig
%      FIGURE, by itself, creates a new FIGURE or raises the existing
%      singleton*.
%
%      H = FIGURE returns the handle to a new FIGURE or the handle to
%      the existing singleton*.
%
%      FIGURE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FIGURE.M with the given input arguments.
%
%      FIGURE('Property','Value',...) creates a new FIGURE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Figure_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Figure_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Figure

% Last Modified by GUIDE v2.5 27-Mar-2016 13:00:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Figure_OpeningFcn, ...
                   'gui_OutputFcn',  @Figure_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Figure is made visible.
function Figure_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Figure (see VARARGIN)

% Choose default command line output for Figure
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Figure wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Figure_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fclose('all');
clc
% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in Parcourir.
function Parcourir_Callback(hObject, eventdata, handles)
% hObject    handle to Parcourir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename, pathname] = uigetfile('*', 'Choisir un fichier');
if  isequal(filename, 0)
    warndlg('vous n''avez rien choisi -_-');
    set(handles.edit1, 'string', '');
else
    set(handles.edit1, 'string', [pathname filename]);
end

% --- Executes on selection change in algorithme.
function algorithme_Callback(hObject, eventdata, handles)
% hObject    handle to algorithme (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns algorithme contents as cell array
%        contents{get(hObject,'Value')} returns selected item from algorithme

% --- Executes during object creation, after setting all properties.
function algorithme_CreateFcn(hObject, eventdata, handles)
% hObject    handle to algorithme (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Coder.
function Coder_Callback(hObject, eventdata, handles)
% hObject    handle to Coder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
algorithme = get(handles.algorithme, 'Value');
setappdata(0, 'algorithme', algorithme);
fullPath = get(handles.edit1, 'string');
type = get(get(handles.radioGroup,'SelectedObject'), 'String');
setappdata(0, 'type', type);
if isempty(fullPath) || isempty(type) || isempty(algorithme)
    warndlg('Veuillez remplir tous les champs');
else
    sequence = convertir(fullPath, type);
    switch algorithme
        case 1
            data = nrz(sequence);
        case 2
            data = nrzi(sequence);
        case 3
            data = manchester(sequence);
        case 4
            data = manchesterDiff(sequence);
        case 5
            data = miller(sequence);
    end
    setappdata(0, 'data', data);
end


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when selected object is changed in radioGroup.
function radioGroup_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in radioGroup 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function radioGroup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to radioGroup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in Decoder.
function Decoder_Callback(hObject, eventdata, handles)
% hObject    handle to Decoder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
y = getappdata(0, 'data');
algorithme = getappdata(0, 'algorithme');
type = getappdata(0, 'type');
if isempty(y)
    warndlg('Il faut coder d''abord!!');
else
    switch algorithme
        case 1
            sequence = dnrz(y);
        case 2
            sequence = dnrzi(y);
        case 3
            sequence = dmanchester(y);
        case 4
            sequence = dmanchesterDiff(y);
        case 5
            sequence = dmiller(y);
    end
    Output = inverser(sequence, type);
    switch type
        case 'texte'
            F = fopen('texte.txt', 'w');
            fprintf(F, '%s', Output);
        case 'image'
            figure
            imshow(uint8(Output))
        case 'audio'
            wavplay(Output)
    end
    fclose('all');
end
