function sequence = dmiller(y)

if isequal(y(1 : 2), [5 -5])
    sequence(1) = '1';
else
    sequence(1) = '0';
end

for i = 3 : 2 : length(y) - 1
    if isequal(y(i : i + 1), [y(i - 1) y(i - 1)])
        sequence(i) = '0';
    else
        if isequal(y(i), y(i - 1))
            sequence(i) = '1';
        else
            sequence(i) = '0';
    end
end

end