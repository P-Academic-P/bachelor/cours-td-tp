
                %----------------------------%
                % remonter au media original %
                %----------------------------%

function Output = inverser(data, type)

switch type
    case 'texte'
        texte = char(bin2dec(reshape(data, 8, [])'));
        Output = texte';
        
    case 'image'
        % extraire l'en-t�te.
        hauteur = bin2dec(data(2:13));
        largeur = bin2dec(data(14:25));
        image = bin2dec(reshape(data(26:end), 8, [])');
        if data(1) == '1' % si l'image est RGB.
            Output = reshape(image, hauteur, largeur, 3);
        else
            Output = reshape(image, hauteur, largeur);
        end
        
    case 'audio'
        
        if data(1) == '1' % si l'audio est st�r�o.
            audio = bin2dec(reshape(data(18:end), 16, [])');
            audio = (audio - 2^15) / 2^15;
            Output = reshape(audio, bin2dec(data(2:17)), 2);
        else
            audio = bin2dec(reshape(data(2:end), 16, [])');
            audio = (audio - 2^15) / 2^15;
            Output = audio;
        end
end