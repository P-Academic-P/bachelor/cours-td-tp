function sequence = dmanchester(y)

for i = 1 : 2 : length(y) - 1
    if isequal(y(i : i + 1), [5 -5])
        sequence(i) = '1';
    else
        sequence(i) = '0';
    end
end
end