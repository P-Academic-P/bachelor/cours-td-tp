
            %-------------------------------------------%
            % convertir un fichier en une suite binaire %
            %-------------------------------------------%
                
function Out = convertir(nomFichier, type)
    
% % Creer le fichier ou on va �crire les suites binaires (l'image et son).
% F = fopen('File.txt', 'w');

switch type
    case 'texte' % un fichier texte.
        Out = reshape(dec2bin(char(importdata(nomFichier)), 8)', 1, []);
%         fprintf(F, '%s', Out);
        
    case 'image' % une image.
        I = imread(nomFichier);
        hauteur = size(I, 1);
        largeur = size(I, 2);
        if length(size(I)) > 2
            Out = [1 dec2bin(hauteur, 12) dec2bin(hauteur, 12) reshape(dec2bin(I, 8)', 1, [])];
%             fprintf(F, '%s', Out);
            % 1 indique qu'il s'agit d'une image trueColor.
            % les dimensions sur 12 bits, car l'image peut d�passer 256 x 256.
        else
            Out = [0 dec2bin(hauteur, 12) dec2bin(hauteur, 12) reshape(dec2bin(I, 8)', 1, [])];
%             fprintf(F, '%s', Out);
            % 0 indique qu'il s'agit d'une image bi-dimensionnel.
        end
        
    case 'audio' % un fichier audio.
        A = wavread(nomFichier);
        longueur = size(A, 1);
        
        if size(A, 2) > 1
            Out = [1 dec2bin(longueur, 16) reshape(dec2bin(A * 2^(15) + 2^(15), 16)', 1, [])];
%             fprintf(F, '%s', Out);
            % 1 indique qu'il s'agit d'un audio st�r�o.
        else
            Out = [0 dec2bin(longueur, 16) reshape(dec2bin(A * 2^(15) + 2^(15), 16)', 1, [])];
%             fprintf(F, '%s', Out);
            % 0 indique qu'il s'agit d'un audio mono.
        end
end
% fclose('all');
end