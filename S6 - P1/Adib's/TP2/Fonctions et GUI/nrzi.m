function y = nrzi(sequence)
v = 5;
y = zeros(1, length(sequence));
if(sequence(1) == '1')
    y(1) = v;
else
    y(1) = -v;
end
for i = 2 : length(sequence)
    switch sequence(i)
        case '1'
            y(i) = y(i - 1);
        case '0'
            y(i) = -y(i - 1);
    end
end
if length(sequence) > 300
    max = 300;
else
    max = length(sequence);
end
stairs(0 : length(sequence), [y y(end)])
axis([0, max, -10, 10])
end