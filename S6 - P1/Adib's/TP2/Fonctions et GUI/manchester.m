function y = manchester(sequence)
v = 5;
y = [];
for i = 1 : length(sequence)
    if sequence(i) == '1'
        y = [y v -v];
    else
        y = [y -v v];
    end
end
if length(sequence) > 300
    max = 300;
else
    max = length(sequence);
end
stairs(0 : 0.5 : length(sequence), [y y(end)]);
axis([0 max + 1 -10 10])
end