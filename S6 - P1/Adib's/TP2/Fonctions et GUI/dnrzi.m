function sequence = dnrzi(y)

if y(1) == 5
    y(1) = '1';
else
    y(1) = '0';
end
for i = 2 : length(y)
    if y(i) == y(i - 1)
        sequence(i) = '1';
    else
        sequence(i) = '0';
    end
end
end