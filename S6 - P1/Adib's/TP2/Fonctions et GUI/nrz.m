function y = nrz(sequence)
v = 5;
y = zeros(1, length(sequence));
for i = 1 : length(sequence)
    if sequence(i) == '1'
        y(i) = v;
    else
        y(i) = -v;
    end
end
if length(sequence) > 300
    max = 300;
else
    max = length(sequence);
end
stairs(0 : length(sequence), [y y(end)])
axis([0, max, -10, 10])
end