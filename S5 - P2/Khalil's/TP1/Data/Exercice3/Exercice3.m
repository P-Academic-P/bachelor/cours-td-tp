%--1)
X = imread('coins.png');
subplot(1, 2, 1), imshow(X)
subplot(1, 2, 2), imhist(X)
%--2)
[C, x] = imhist(X);
C(100)
%--3)
X1 = 255 - X;
subplot(2, 2, 1), imshow(X)
subplot(2, 2, 2), imhist(X)
subplot(2, 2, 3), imshow(X1)
subplot(2, 2, 4), imhist(X1)
%--4)
subplot(2, 2, 1), imshow(im2bw(X, 0.39))
subplot(2, 2, 2), imshow(im2bw(X, 0.5))
subplot(2, 2, 3), imshow(im2bw(X, 0.58))
%--5)
Level = graythresh(X);
imshow(im2bw(X, Level))
%--6)
Y = (255 / (max(max(X)) - min(min(X)))) * (X - min(min(X))) ;
subplot(1, 2, 1), imshow(Y)
subplot(1, 2, 2), imhist(Y)
%--7)
I = histeq(X);
J = adapthisteq(X);
subplot(1, 2, 1), imshow(I)
subplot(1, 2, 2), imhist(I)
figure;
subplot(1, 2, 1), imshow(J)
subplot(1, 2, 2), imhist(J)
%--8)
A = adapthisteq(X, 'Distribution', 'uniform');
B = adapthisteq(X, 'Distribution', 'rayleigh');
C = adapthisteq(X, 'Distribution', 'exponential');
subplot(3, 2, 1), imshow(A)
subplot(3, 2, 2), imhist(A)
subplot(3, 2, 3), imshow(B)
subplot(3, 2, 4), imhist(B)
subplot(3, 2, 5), imshow(C)
subplot(3, 2, 6), imhist(C)