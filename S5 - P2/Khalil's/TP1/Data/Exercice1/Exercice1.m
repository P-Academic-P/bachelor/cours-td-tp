%--1)
X = imread('cameraman.tif');
imshow(X)
%--2)
imshow(X(25, :))
%--3)
imshow(X(:, 150))
%--4)
subplot(2, 2, 1), imshow(X)
subplot(2, 2, 2), imshow(X(25, :))
subplot(2, 2, 3), imshow(X(:, 150))
%--5)
figure;
subplot(2, 2, 1), imshow(imadd(X, 50))
subplot(2, 2, 2), imshow(imadd(X, 100))
subplot(2, 2, 3), imshow(imadd(X, 150))
%--6)
figure;
subplot(2, 2, 1), imshow(imsubtract(X, 50))
subplot(2, 2, 2), imshow(imsubtract(X, 100))
subplot(2, 2, 3), imshow(imsubtract(X, 150))
%--7)
figure;
subplot(2, 2, 1), imshow(X)
subplot(2, 2, 2), imshow(immultiply(X, 2))
subplot(2, 2, 3), imshow(X)
subplot(2, 2, 4), imshow(imdivide(X, 4))
%--8)
figure;
Y = imread('toycars1.png');
Z = imread('toycars2.png');
Yb = im2bw(Y);
Zb = im2bw(Z);
subplot(2, 2, 1), imshow(and(Yb, Zb))
subplot(2, 2, 2), imshow(or(Yb, Zb))
subplot(2, 2, 3), imshow(xor(Yb, Zb))
%--9)
figure;
subplot(2, 2, 1), imshow(imrotate(X, -90))
subplot(2, 2, 2), imshow(imrotate(X, -45))
subplot(2, 2, 3), imshow(imrotate(X, 45))
subplot(2, 2, 4), imshow(imrotate(X, 90))
%--10)
figure;
subplot(2, 2, 1), imshow(imresize(X, 0.1))
subplot(2, 2, 2), imshow(imresize(X, 0.5))
subplot(2, 2, 3), imshow(imresize(X, 1.5))
subplot(2, 2, 4), imshow(imresize(X, 2.5))