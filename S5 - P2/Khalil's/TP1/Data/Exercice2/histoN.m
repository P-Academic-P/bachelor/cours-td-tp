function[hiN] = histoN(Picname)
Mat = imread(Picname);
Mat = Mat(:, :, 1);
[n, l] = size(Mat);
hiN = histo(Picname)/(n * l);
bar(hiN)
end