%--1)
X = imread('bureau.jpg');
X = X(:, :, 1);
imshow(X)
%--2)
 %--(a)
 X1 = 255 - X;
 subplot(1, 2, 1), imshow(X)
 subplot(1, 2, 2), imshow(X1)
 %--(b)
 [n, l] = size(X);
 X2 = zeros(size(X));
 for i = 1:n
     for j = 1:l
         if X(i, j) > 128
             X2(i, j) = 255;
         else
             X2(i, j) = 0;
         end
     end
 end
 subplot(1, 2, 1), imshow(X)
 subplot(1, 2, 2), imshow(X2)
 %--(c)
 k = 1;
 for c = 1:4
     X3 = c * log(1 + im2double(X));
     subplot(2, 2, k); imshow(X3)
     k = k + 1;
 end
 imshow(X3)
 %--(d)
 for c = 0.1:0.1:0.4
     k = 1;
     figure;
     for d = 0.1:0.1:0.4
         X4 = c * ((1 + d).^(X) + 1);
         subplot(2, 2, k); imshow(X3)
         k = k + 1;
     end
 end
 %--(e)
 k = 1;
 for c = 4:8
     X5 = X.^c;
     subplot(2, 2, k); imshow(X5)
     k = k + 1;
 end
