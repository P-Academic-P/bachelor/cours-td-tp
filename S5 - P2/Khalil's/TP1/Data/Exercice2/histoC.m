function[hiC] = histoC(Picname)
hiN = histoN(Picname);
hiC = zeros(size(hi));
for i = 1:256
    hiC(i) = sum(hiN(1:i));
end
bar(hiC)
end