function[hi] = histo(Picname)
Mat = imread(Picname);
Mat = Mat(:, :, 1);
[n, l] = size(Mat);
hi = zeros(256, 1);
for a = 1:n
    for b = 1:l
        hi(Mat(a, b) + 1) = hi(Mat(a, b) + 1) + 1;
    end
end
bar(hi)
end