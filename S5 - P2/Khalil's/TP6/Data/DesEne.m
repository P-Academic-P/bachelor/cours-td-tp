%%  Energie.
    function Y = DesEne( X )
    X = im2double( X );
    Y = sum( sum( X .^ 2 ) );
    end