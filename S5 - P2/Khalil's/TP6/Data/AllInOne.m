%%  Cr�ation de la base des donn�es & la matrice des histogrammes.
    function [ M , H ] = AllInOne( N )
    
    M = uint8(zeros( 128 , 128 , N ));
    H = zeros( 256 , N );
    
    for k = 1 : N
        M( : , : , k ) = imread( [ 'im/' int2str(k) '.png' ] );
        H( : , k ) = imhist( M( : , : , k ) );
    end
    end