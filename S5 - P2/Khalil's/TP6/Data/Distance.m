%%  Calculer la distance.
    function [ D ] = Distance( X1 , X2 )
        D = sqrt( sum( ( X1 - X2 ) .^2 ) );
    end