%%  Cr�ation de la matrice des distances.
    function D = MatDistanceV2( M , T )
    
    D = zeros( 108 , 108 );
    
    if nargin == 2;
        X1 = CalculBlocks( M , T );
        c = 1;
        for a = 1 : 2 : 215
            d = 0;
            for b = 2 : 2 : 216
                d = d + 1;
                D( d , c ) = Distance( X1(: , a) , X1(: , b) );
            end
            c = c + 1;
        end
    else
        c = 1;
        for a = 1 : 2 : 215
            d = 0;
            for b = 2 : 2 : 216
                d = d + 1;
                D( d , c ) = Distance( M( : , a ) , M( : , b ) );
            end
            c = c + 1;
        end
    end