%%  Retourner le vecteur d'un descripteur pour tous les images de la matrice.
    function Z = CalculBlocks( M , T )
    for i = 1:216
        fun = @(block_struct) DesVar( block_struct.data );
        Y = blockproc( M( : , : , i ) , [ T T ] , fun );
        Y = reshape( Y , size(Y , 1)^2 , 1 );
        Z(: , i) = Y;
    end