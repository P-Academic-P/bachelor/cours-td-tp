%%  Contraste.
    function Y = DesCon( X )
    [ l, c ] = size( X );
    X = im2double( X );
    Y = 0;
    for a = 1 : l
        for b = 1 : c
            Y = Y + X( a , b ) * ( a - b ) ^ 2;
        end
    end
    end