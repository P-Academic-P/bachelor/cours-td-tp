    function CMC = CalculCMC(D)
    Rvect = 1 : 108;
    Cvect = 1 : 108;
    
    [ ~ , Index ] = sort( D );
    Comp = repmat( Rvect , [ length(Rvect) 1 ] );
    Res_comp = ( Comp == Index );
    
    Rangs = zeros( 108 , 1 );
    for i = 1 : length( Rvect )
        Rangs( i ) = sum( sum( Res_comp( 1 : i , : ) ) );
    end
    CMC = Rangs / length(Cvect);