%%  Entropie.
    function Y = DesEnt( X )
    X = im2double( X );
    Y = - sum( sum( im2double( X ) .* log2( im2double( X ) ) ) );
    end