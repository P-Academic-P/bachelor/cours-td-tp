%%  Cr�ation de la matrice des distances.
    function D = MatDistance( M , T )
    
    D = zeros( 108 , 108 );
    
    if nargin == 2;
        c = 1;
        for a = 1 : 2 : 215
            d = 0;
            for b = 2 : 2 : 216
                d = d + 1;
                X1 = CalculBlocks( M( : , : , a ) , T );
                X2 = CalculBlocks( M( : , : , b ) , T );
                D( d , c ) = Distance( X1 , X2 );
            end
            c = c + 1;
        end
        
    else
        c = 1;
        for a = 1 : 2 : 215
            d = 0;
            for b = 2 : 2 : 216
                d = d + 1;
                D( d , c ) = Distance( M( : , a ) , M( : , b ) );
            end
            c = c + 1;
        end
    end