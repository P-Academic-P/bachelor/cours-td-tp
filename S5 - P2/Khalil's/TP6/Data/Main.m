%%%  Main
tic
    [ M , H ] = AllInOne( 216 );

%%  L'histogramme :
%     CMC = CalculCMC(MatDistance( H ));
%     plot(CMC, '-ro'); title('Histogramme')
%%  L'un des descripteurs :
    CMC = CalculCMC(MatDistanceV2( M , 8 ));
    CMC1 = CalculCMC(MatDistanceV2( M , 16 ));
    CMC2 = CalculCMC(MatDistanceV2( M , 32 ));
    CMC3 = CalculCMC(MatDistanceV2( M , 64 ));
    CMC4 = CalculCMC(MatDistanceV2( M , 128 ));
    plot(1:108, CMC, 'red', 1:108, CMC1, 'blue', 1:108, CMC2, 'yellow', 1:108, CMC3, 'green', 1:108, CMC4, 'black');
    legend('8 x 8', '16 x 16', '32 x 32', '64 x 64', '128 x 128');
    title('Variance');
toc