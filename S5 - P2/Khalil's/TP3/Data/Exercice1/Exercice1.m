%% 

%1-Lire l'image 'onion.png' et convertir-la en niveaux de gris � l'aide de la fonction rgb2gray. Afficher les deux images r�sultantes.
X = imread('onion.png');
Y = rgb2gray(X);
subplot(1, 2, 1); imshow(X)
subplot(1, 2, 2); imshow(Y)

%2-Extraire les 3 canaux de l'image RGB "onio.png" et afficher les avec %l'image originale.
Xr = X(:, :, 1);
Xg = X(:, :, 2);
Xb = X(:, :, 3);
subplot(2, 2, 1); imshow(X)
subplot(2, 2, 2); imshow(Xr)
subplot(2, 2, 3); imshow(Xg)
subplot(2, 2, 4); imshow(Xb)

%3-Convertir l'image onion.png en image HSV et afficher la.
Z = rgb2hsv(X);
subplot(1, 2, 1); imshow(X)
subplot(1, 2, 2); imshow(Z)

%4-Extraire les 3 canaux de l'image HSV et afficher-les.
Zr = Z(:, :, 1);
Zg = Z(:, :, 2);
Zb = Z(:, :, 3);
subplot(2, 2, 1); imshow(Z)
subplot(2, 2, 2); imshow(Zr)
subplot(2, 2, 3); imshow(Zg)
subplot(2, 2, 4); imshow(Zb)

%5-Convertir l'image onion.png en un image ycbcr et afficher la.	
W = rgb2ycbcr(X);
subplot(1, 2, 1); imshow(X)
subplot(1, 2, 2); imshow(W)

%6-Extraire les 3 canaux de l'image ycbr et afficher-les.
Wr = X(:, :, 1);
Wg = X(:, :, 2);
Wb = X(:, :, 3);
subplot(2, 2, 1); imshow(W)
subplot(2, 2, 2); imshow(Wr)
subplot(2, 2, 3); imshow(Wg)
subplot(2, 2, 4); imshow(Wb)