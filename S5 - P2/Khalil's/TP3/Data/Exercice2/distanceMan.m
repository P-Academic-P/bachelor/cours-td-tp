function [value1, value2, value3] = distanceMan(X1, X2)

[rHist1, gHist1, bHist1] = rgbhist(X1);
[rHist2, gHist2, bHist2] = rgbhist(X2);

 value1 = sum(abs(rHist1 - rHist2));
 value2 = sum(abs(gHist1 - gHist2));
 value3 = sum(abs(bHist1 - bHist2));
 
end