%% Question supp. HSV
[filename, pathname] = uigetfile('images2/*.bmp', 'Pick an Image');
if  isequal(filename, 0) || isequal(pathname, 0)
    warndlg('Image is not selected');
else
    X = imread(['images2/' filename]);
    X = imnoise(X, 'gaussian', 0.02);
end
resultValues1 = zeros(50, 1);
resultValues2 = zeros(50, 1); 
resultValues3 = zeros(50, 1); 
resultNames = {};

i = 1;
while i <= 50
    Y = imread(['images2/' int2str(i) '.bmp']);
    Y = rgb2hsv(Y);
    [resultValues1(i), resultValues2(i), resultValues3(i)] = distance(X, Y);
    resultNames{i} = ['images2/' int2str(i) '.bmp'];
    i = i + 1;
end

%Trier
[sortedValues1, index1] = sort(resultValues1);
[sortedValues2, index2] = sort(resultValues2);
[sortedValues3, index3] = sort(resultValues3);

Final = (resultValues1 + resultValues2 + resultValues3)/3;
Final = sort(Final)

%------------------Hue------------------------------------------------
figure('name', 'Hue')
for i = 1:8
    tempstr = char(resultNames(index1(i)));
    subplot(2, 4, i), imshow(tempstr); title(['Image N� ' int2str(index1(i))])
end

%------------------Saturation----------------------------------------------
figure('name', 'Saturation')
for i = 1:8
    tempstr = char(resultNames(index2(i)));
    subplot(2, 4, i), imshow(tempstr); title(['Image N� ' int2str(index2(i))])
end

%------------------Value-----------------------------------------------
figure('name', 'Value')
for i = 1:8
    tempstr = char(resultNames(index3(i)));
    subplot(2, 4, i), imshow(tempstr); title(['Image N� ' int2str(index3(i))])
end