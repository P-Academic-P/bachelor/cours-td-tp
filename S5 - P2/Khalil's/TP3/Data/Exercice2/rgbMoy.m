function[Rmoy, Gmoy, Bmoy] = rgbMoy(X)
[l, c] = size(X);
Rmoy = sum(sum(X(:, :, 1))) / (l * c);
Gmoy = sum(sum(X(:, :, 2))) / (l * c);
Bmoy = sum(sum(X(:, :, 3))) / (l * c);
end