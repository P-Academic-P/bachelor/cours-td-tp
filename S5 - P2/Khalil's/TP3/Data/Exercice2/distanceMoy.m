function[dR, dG, dB] = distanceMoy(X, Y)
[R1, G1, B1] = rgbMoy(X);
[R2, G2, B2] = rgbMoy(Y);
dR = abs(R1 - R2);
dG = abs(G1 - G2);
dB = abs(B1 - B2);
end