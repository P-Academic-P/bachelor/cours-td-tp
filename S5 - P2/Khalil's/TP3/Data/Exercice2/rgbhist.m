%%
function [rHist ,gHist, bHist] = rgbhist(X)

rHist = imhist(X(:, :, 1));
gHist = imhist(X(:, :, 2));
bHist = imhist(X(:, :, 3));
return