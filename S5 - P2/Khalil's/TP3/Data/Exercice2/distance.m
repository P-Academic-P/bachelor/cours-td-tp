function [value1, value2, value3] = distance(X1, X2)

[rHist1, gHist1, bHist1] = rgbhist(X1);
[rHist2, gHist2, bHist2] = rgbhist(X2);

 value1 = sqrt(sum((rHist1 - rHist2).^2));
 value2 = sqrt(sum((gHist1 - gHist2).^2));
 value3 = sqrt(sum((bHist1 - bHist2).^2));
 
end