function[MSE] = MeanSquareError(X, Y)
[N, M]=size(X);
MSE = sum(sum((Y - X).^2)) / (N * M);
end