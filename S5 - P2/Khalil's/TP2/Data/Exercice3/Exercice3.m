%--1)
X = imread('cameraman.tif');

%--2)
figure;
X1 = imnoise(X, 'gaussian', 0.02);
X2 = imnoise(X, 'gaussian', 0.05);
X3 = imnoise(X, 'gaussian', 0.1);
X4 = imnoise(X, 'salt & pepper', 0.02);
X5 = imnoise(X, 'salt & pepper', 0.05);
X6 = imnoise(X, 'salt & pepper', 0.1);
subplot(2, 3, 1), imshow(X1); title('Gaussian 0.02');
subplot(2, 3, 2), imshow(X2); title('Gaussian 0.05');
subplot(2, 3, 3), imshow(X3); title('Gaussian 0.1');
subplot(2, 3, 4), imshow(X4); title('S&P 0.02');
subplot(2, 3, 5), imshow(X5); title('S&P 0.05');
subplot(2, 3, 6), imshow(X6); title('S&P 0.1');

%--3)
figure;
subplot(2, 3, 1), imshow(imfilter(X1, fspecial('average', 5)))
subplot(2, 3, 2), imshow(imfilter(X2, fspecial('average', 5)))
subplot(2, 3, 3), imshow(imfilter(X3, fspecial('average', 5)))
subplot(2, 3, 4), imshow(imfilter(X4, fspecial('average', 5)))
subplot(2, 3, 5), imshow(imfilter(X5, fspecial('average', 5)))
subplot(2, 3, 6), imshow(imfilter(X6, fspecial('average', 5)))

%--4)
figure;
subplot(2, 3, 1), imshow(imfilter(X1, fspecial('gaussian', 5)))
subplot(2, 3, 2), imshow(imfilter(X2, fspecial('gaussian', 5)))
subplot(2, 3, 3), imshow(imfilter(X3, fspecial('gaussian', 5)))
subplot(2, 3, 4), imshow(imfilter(X4, fspecial('gaussian', 5)))
subplot(2, 3, 5), imshow(imfilter(X5, fspecial('gaussian', 5)))
subplot(2, 3, 6), imshow(imfilter(X6, fspecial('gaussian', 5)))

%--5)
figure;
subplot(2, 3, 1), imshow(medfilt2(X1, [5 5]))
subplot(2, 3, 2), imshow(medfilt2(X2, [5 5]))
subplot(2, 3, 3), imshow(medfilt2(X3, [5 5]))
subplot(2, 3, 4), imshow(medfilt2(X4, [5 5]))
subplot(2, 3, 5), imshow(medfilt2(X5, [5 5]))
subplot(2, 3, 6), imshow(medfilt2(X6, [5 5]))