function[PSNR] = PeakSignalToNoiseRatio(MSE)
PSNR = 10 * log10((256^2) / MSE);
end