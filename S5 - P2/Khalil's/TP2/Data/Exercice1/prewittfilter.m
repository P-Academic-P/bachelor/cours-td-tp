function[] = prewittfilter(Image)
X = imread(Image);
X = im2double(X);
[l, c] = size(X);
Prewittx = zeros(size(X));
Prewitty = zeros(size(X));
Masquey = [-1 -1 -1; 0 0 0; 1 1 1];
Masquex = [-1 0 1; -1 0 1; -1 0 1];
for a = 2:l - 1
    for b = 2:c - 1
        Prewittx(a, b) = sum(sum(X(a - 1:a + 1, b - 1:b + 1).*Masquex))/6;
        Prewitty(a, b) = sum(sum(X(a - 1:a + 1, b - 1:b + 1).*Masquey))/6;
    end
end
Y = (abs(Prewittx) + abs(Prewitty))/2;
figure,
subplot(1, 2, 1), imshow(Y)
graythresh(Y)
Z = im2bw(Y, graythresh(Y));
subplot(1, 2, 2), imshow(Z)
end