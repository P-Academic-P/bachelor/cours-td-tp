function[] = meanfilter(Image)
X = imread(Image);
[l, c] = size(X);
Y = X;
Y(1, :) = 0;
Y(:, 1) = 0;
Y(end, :) = 0;
Y(:, end) = 0;
for a = 2:l - 1
    for b = 2:c - 1
        Y(a, b) = floor(sum(sum(X(a - 1:a + 1, b - 1:b + 1)))/9);
    end
end
figure;
subplot(1, 2, 1), imshow(X)
subplot(1, 2, 2), imshow(Y)
end