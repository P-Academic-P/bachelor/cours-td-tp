function[] = sobelfilter(Image)
X = imread(Image);
X = im2double(X);
[l, c] = size(X);
Sobelx = zeros(size(X));
Sobely = zeros(size(X));
Masquex = [-1 0 1; -2 0 2; -1 0 1];
Masquey = [-1 -2 -1; 0 0 0; 1 2 1];
for a = 2:l - 1
    for b = 2:c - 1
        Sobelx(a, b) = sum(sum(X(a - 1:a + 1, b - 1:b + 1).*Masquex))/8;
        Sobely(a, b) = sum(sum(X(a - 1:a + 1, b - 1:b + 1).*Masquey))/8;
    end
end
Y = (abs(Sobelx) + abs(Sobely))/2;
figure,
subplot(1, 2, 1), imshow(Y)
graythresh(Y)
Z = im2bw(Y, graythresh(Y));
subplot(1, 2, 2), imshow(Z)
end