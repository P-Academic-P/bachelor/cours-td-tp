function[] = laplacianfilter(Image)
X = imread(Image);
X = im2double(X);
Y = zeros(size(X));
[l, c] = size(X);
Masque = [1 1 1; 1 -8 1; 1 1 1];
for a = 2:l - 1
    for b = 2:c - 1
        Y(a, b) = sum(sum(X(a - 1:a + 1,b - 1:b + 1) .* Masque))/9;
    end
end
figure,
subplot(1, 2, 1), imshow(Y)
graythresh(Y)
Z = im2bw(Y, graythresh(Y));
subplot(1, 2, 2), imshow(Z)
end