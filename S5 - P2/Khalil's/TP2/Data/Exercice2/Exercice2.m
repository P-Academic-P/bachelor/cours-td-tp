%--1)
X = imread('circuit.tif');

%--2)
figure,
subplot(1, 2, 1), imshow(X)
subplot(1, 2, 2), imshow(edge(X, 'sobel'));

%--3)
figure,
subplot(3, 2, 1), imshow(X)
subplot(3, 2, 2), imshow(edge(X, 'prewitt', 0.01)); title('Seuil = 0,01');
subplot(3, 2, 3), imshow(edge(X, 'prewitt', 0.03)); title('Seuil = 0,03');
subplot(3, 2, 4), imshow(edge(X, 'prewitt', 0.05)); title('Seuil = 0,05');
subplot(3, 2, 5), imshow(edge(X, 'prewitt', 'horizontal')); title('Direction horizontal');
subplot(3, 2, 6), imshow(edge(X, 'prewitt', 'vertical')); title('Direction vertical');

%--4)
figure,
subplot(3, 2, 1), imshow(X)
subplot(3, 2, 2), imshow(edge(X, 'roberts', 0.01)); title('Seuil = 0,01');
subplot(3, 2, 3), imshow(edge(X, 'roberts', 0.03)); title('Seuil = 0,03');
subplot(3, 2, 4), imshow(edge(X, 'roberts', 0.05)); title('Seuil = 0,05');
subplot(3, 2, 5), imshow(edge(X, 'roberts', 'horizontal')); title('Direction horizontal');
subplot(3, 2, 6), imshow(edge(X, 'roberts', 'vertical')); title('Direction vertical');

%--5)
figure,
subplot(2, 2, 1), imshow(imfilter(X, fspecial('sobel'))); title('sobel(imfilter)');
subplot(2, 2, 2), imshow(edge(X, 'sobel')); title('sobel(edge)');
subplot(2, 2, 3), imshow(imfilter(X, fspecial('prewitt'))); title('prewitt(imfilter)');
subplot(2, 2, 4), imshow(edge(X, 'prewitt')); title('prewitt(edge)');

%--6)
figure,
k = 1;
for i = [0.1 0.2 0.4 0.8]
    subplot(2, 2, k), imshow(imfilter(X, fspecial('laplacian', i)))
    title(['alpha = ' num2str(i)]);
    k = k + 1;
end
