%%  Calculer la distance.
    function [ D ] = Distance( X1 , X2 , T )
        D = sqrt( sum( sum( ( CalDis( X1 , T ) - CalDis( X2 , T ) ) .^2 ) ) );
    end