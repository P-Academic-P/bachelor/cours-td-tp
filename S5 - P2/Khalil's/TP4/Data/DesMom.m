function Y = DesMom( X )
[ l , c ] = size( X );
X = im2double( X );
Y = 0;
for a = 1 : l
    for b = 1 : c
        Y = Y + X( a , b ) / (1 + abs( a - b ) );
    end
end
end