%% Chercher une image dans la BD.
 %% Ouverture de l'image :
 tic
 [filename, pathname] = uigetfile('images1/*.tif', 'Pick an Image');
 if  isequal(filename, 0) || isequal(pathname, 0)
     warndlg('Image is not selected');
 else
     X = imread(['images1/' filename]);
     imshow(X)
     X = im2double(rgb2gray(X));
 end

 %% Trouver le vecteur :
 Values = zeros(1, 20);
 G = AllInOne(20);
 k = 1;
 
 while k <= 20
     Values(k) = Distance(X, G(:, :, k), 128);
     k = k + 1;
 end
 %% Affichage des 4 premiers images :
 [SortedValues, Index] = sort(Values)

 figure;
 for l = 1:4
     subplot(2, 2, l), imshow(['images1/D' int2str(Index(l)) '_COLORED.tif']), title(['Image N�' int2str(Index(l))]);
 end
 toc