function Y = DesVar( X )
[ l , c ] = size( X );
X = im2double( X );
m = mean2( X );
Y = sum( sum( ( X - m ) .^ 2 ) ) / ( l * c );
end