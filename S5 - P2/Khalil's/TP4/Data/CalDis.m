%% Retourner le vecteur d'un descripteur.
function Z = CalDis(X, T)
        fun = @(block_struct) DesVar( block_struct.data );
        Z = blockproc( X , [ T T ] , fun );
        Z = reshape( Y , size(Y , 1)^2 , 1 );
end